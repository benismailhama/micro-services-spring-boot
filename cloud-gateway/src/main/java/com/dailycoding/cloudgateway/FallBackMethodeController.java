package com.dailycoding.cloudgateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackMethodeController {

	@GetMapping("/userServiceFallBack")
	public String userServiceFallBackMethod() {
		
		return "User service is taking longer than expected";
	}
	
	@GetMapping("/deparmentServiceFallBack")
	public String deparmentServiceFallBackMethod() {
		
		return "Department service is taking longer than expected";
	}
}
