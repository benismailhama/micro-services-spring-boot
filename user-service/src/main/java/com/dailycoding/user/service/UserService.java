package com.dailycoding.user.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dailycoding.user.VO.Department;
import com.dailycoding.user.VO.ResponseTemplateVO;
import com.dailycoding.user.entity.User;
import com.dailycoding.user.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserService {

	
	private final  UserRepository userRepository;

	private final RestTemplate restTemplate;
	
	public User saveUser(User user) {
		return userRepository.save(user);
	}

	public ResponseTemplateVO getUserWithDepartment(Long userId) {
		
		ResponseTemplateVO vo = new ResponseTemplateVO();
		
		User user = userRepository.findById(userId).orElse(null);
		
		Department department = restTemplate.getForObject("http://DEPARTMENT-SERVICE/departments/"+user.getDepartmentId(), Department.class);
		
		vo.setUser(user);
		vo.setDepartment(department);
		
		return vo;
		
	}
	
	
	
}
