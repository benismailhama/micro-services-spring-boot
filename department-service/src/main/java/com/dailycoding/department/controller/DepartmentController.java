package com.dailycoding.department.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dailycoding.department.entity.Department;
import com.dailycoding.department.service.DepartmentService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@AllArgsConstructor
@RequestMapping("/departments")
@Slf4j
public class DepartmentController {

	private final DepartmentService departmentService;
	
	@PostMapping("/add")
	public Department saveDepartment(@RequestBody Department department) {
	
		return departmentService.saveDepartment(department);
	}
	
	@GetMapping("/{id}")
	public Department findDepartmentById(@PathVariable("id") Long deparmentId) {
		return departmentService.findDepartmentById(deparmentId);
	}
}
