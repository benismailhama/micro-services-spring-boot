package com.dailycoding.department.service;

import org.springframework.stereotype.Service;

import com.dailycoding.department.entity.Department;
import com.dailycoding.department.repository.DepartmentRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class DepartmentService {

	private final DepartmentRepository departmentRepository;

	public Department saveDepartment(Department department) {
		return  departmentRepository.save(department);
	}

	public Department findDepartmentById(Long deparmentId) {
		return departmentRepository.findById(deparmentId).orElse(null);
	}
}
